GIT_SHA1                := $(shell git rev-parse --short HEAD)
export VCS_REF          := $(GIT_SHA1)
export BUILD_DATE       := $(shell date -u +'%Y-%m-%dT%H:%M:%SZ')
export BUILD_VERSION    := $(SAMBA_VERSION)
export BUILD_TYPE       = stable
export SAMBA_VERSION    = 4.15.5-r0
export SAMBA_IMAGE_NAME = redmoosetech/samba-ad-dc
export ALPINE_IMAGE     = alpine
export ALPINE_VERSION   = 3.15.0
DC1_HOST                = "tcp://172.16.20.200:2376"
DC2_HOST                = "tcp://172.16.20.201:2376"

.PHONY: build all deploy_primary deploy_samba_storage deploy_secondary
default: build
all: build deploy_primary deploy_samba_storage deploy_secondary
deploy_all: deploy_primary deploy_secondary deploy_samba_storage

build:
	docker build \
	--build-arg ALPINE_IMAGE=$(ALPINE_IMAGE) \
	--build-arg ALPINE_VERSION=$(ALPINE_VERSION) \
	-t $(SAMBA_IMAGE_NAME):$(SAMBA_VERSION) \
	-t $(SAMBA_IMAGE_NAME):latest \
	samba-ad-dc/
	docker push $(SAMBA_IMAGE_NAME):$(SAMBA_VERSION)
	docker push $(SAMBA_IMAGE_NAME):latest

#deploy_primary: export DOCKER_HOST:=$(DC1_HOST)
#deploy_primary: export DOCKER_TLS=1
deploy_primary:
	docker-compose -f compose/dc1.yml pull
	docker-compose -f compose/dc1.yml up -d

deploy_secondary: export DOCKER_HOST:=$(DC2_HOST)
deploy_secondary: export DOCKER_TLS=1
deploy_secondary:
	docker-compose pull -f compose/dc2.yml
	docker-compose up -d -f compose/dc2.yml

deploy_samba_storage: export DOCKER_HOST:=$(SMB_HOST)
deploy_samba_storage: export DOCKER_TLS=1
deploy_samba_storage:
	docker-compose pull -f compose/samba.yml
	docker-compose up -d -f compose/samba.yml
