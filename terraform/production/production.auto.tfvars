machines = {
 dc1 = {
  folder = "Infrastructure/Production/Domain"
  template = "rocky8.5_server_amd64_template"
  hostname = "dc1"
  domain = "rmt.local"
  network = "Domain"
  ipv4_address = "172.16.20.200"
  ipv4_netmask = 24
  ipv4_gateway = "172.16.20.254"
  dns_servers = ["172.16.0.17", "172.16.0.18"]
  dns_domain = "rmt"
  num_cpus = 2
  num_cores_per_socket = 2
  memory = 2048
  datastore_1 = "esx0_local_storage"
  datastore_2 = "Storage"
  disks = []
  },
 dc2 = {
  folder = "Infrastructure/Production/Domain"
  template = "rocky8.5_server_amd64_template"
  hostname = "dc2"
  domain = "rmt.local"
  network = "Domain"
  ipv4_address = "172.16.20.201"
  ipv4_netmask = 24
  ipv4_gateway = "172.16.20.254"
  dns_servers = ["172.16.0.17", "172.16.0.18"]
  dns_domain = "rmt"
  num_cpus = 2
  num_cores_per_socket = 2
  memory = 2048
  datastore_1 = "esx0_local_storage"
  datastore_2 = "Storage"
  disks = []
  }
}
