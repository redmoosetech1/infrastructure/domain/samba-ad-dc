#!/bin/bash

INIT_FILE="/etc/samba/.initialized"
# set -eo pipefail

while true
do
  if [ ! -f ${INIT_FILE} ]
  then
    sleep 5s
  else
    break
  fi
done


if /usr/bin/smbclient \
  --configfile=/etc/samba/smb.conf \
  --authentication-file=/.smbclient.conf \
  --list=localhost \
  --max-protocol=SMB2
then
  exit 0
fi

exit 1