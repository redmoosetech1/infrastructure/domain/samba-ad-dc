#!/bin/bash

# Set ENV
INIT_FILE="/etc/samba/.initialized"
SAMBA_ROLE="${SAMBA_ROLE:=primary_domain}"
SAMBA_DEBUGLEVEL=${SAMBA_DEBUGLEVEL:=0}
HOSTNAME=$(hostname -f)
VAULT_ADDR=${VAULT_ADDR:=https://vault.rmt}
VAULT_URI=${VAULT_URI:=/v1/domain/data/users}
VAULT_URL="${VAULT_ADDR}${VAULT_URI}"

# Import log functions
. /init/log_output.sh

# Exit Routines
die() {
    local message=$1
    log_error "$message" >&2
    exit 1
}

finish() {
  rv=$?
  log_INFO "exit with signal '${rv}'"

  if [[ ${rv} -gt 0 ]]
  then
    sleep 4s
  fi

  if [[ "${DEBUG}" = "true" ]]
  then
    caller
  fi

#  log_info ""

  exit ${rv}
}

trap finish SIGINT SIGTERM INT TERM EXIT

kerberos_setup() {
  log_info "Configure Krb5.conf"
  cp /templates/krb5.conf.tpl /etc/krb5.conf
  cat /var/lib/samba/private/krb5.conf >> /etc/krb5.conf
  cat << EOF >> /etc/krb5.conf
    ticket_lifetime = 24h
    renew_lifetime = 7d
    forwardable = true
    rdns = false
EOF
}

get_admin_creds() {
  log_debug "Running in Debug Mode ..."
  log_debug "ROLE-ID: $SAMBA_DC_ADMIN_APP_ROLE_ID"
  log_debug "ROLE-SECRET: $SAMBA_DC_ADMIN_APP_ROLE_SECRET"
  cat << EOF > /tmp/payload.json
  {
    "role_id": "$SAMBA_DC_ADMIN_APP_ROLE_ID",
    "secret_id": "$SAMBA_DC_ADMIN_APP_ROLE_SECRET"
  }
EOF
  log_debug "Was json paylog created ..."
  log_debug "$(cat /tmp/payload.json)"
  APP_TOKEN=$(curl -s -X POST --data @/tmp/payload.json "${VAULT_ADDR}/v1/auth/approle/login" | jq -r .auth.client_token)
  if [[ -z "$APP_TOKEN" ]]; then
    die "Variable APP_TOKEN is empty!"
  fi

  log_debug "APP Token: $APP_TOKEN"
  SAMBA_DC_ADMIN_PASSWD=$(curl -s -H "X-Vault-Token: $APP_TOKEN" "$VAULT_URL" | jq -r .data.data.administrator)
  if [[ -z "$SAMBA_DC_ADMIN_PASSWD" ]]; then
    die "APP Token Error with token: ${SAMBA_DC_ADMIN_PASSWD} ..."
  fi

  log_debug "Admin Password: $SAMBA_DC_ADMIN_PASSWD"
  rm /tmp/payload.json
}

# If first start up get vault domain admin secret
create_primary_domain_controller() {
  log_info "Initializing Primary Domain Controller ..."
  log_info "Getting Administrator Credentials from Vault"

  get_admin_creds

  # Backup Old smb.conf
  mv /etc/samba/smb.conf /etc/samba/smb.conf.bak

  /usr/bin/samba-tool domain provision \
    --server-role=dc \
    --domain="${SAMBA_DC_DOMAIN}" \
    --realm="${SAMBA_DC_REALM}" \
    --adminpass="${SAMBA_DC_ADMIN_PASSWD}" \
    --dns-backend="SAMBA_INTERNAL" \
    --use-rfc2307 \
    --function-level=2008_R2 \
    || die "${SAMBA_DC_DOMAIN} - Domain NOT Provisioned! See Log ..."

  log_info "${SAMBA_DC_DOMAIN} - Domain Provisioned Successfully!"

  # Setting up Kerberos
  kerberos_setup

  echo 'root = administrator' > /etc/samba/smbusers
  # Mark samba as setup
  touch "${INIT_FILE}"
}

create_secondary_domain_controller() {
  log_info "Initializing Primary Domain Controller ..."
  log_info "Getting Administrator Credentials from Vault"

  get_admin_creds
  # Backup Old smb.conf
  mv /etc/samba/smb.conf /etc/samba/smb.conf.bak

  # Setting up Kerberos
  kerberos_setup

  echo "${SAMBA_DC_ADMIN_PASSWD}" | \
  samba-tool domain join "$SAMBA_DC_REALM" DC -U "Administrator" \
  || die "${SAMBA_DC_DOMAIN} - Domain NOT Provisioned! See Log ..."

  log_info "${SAMBA_DC_DOMAIN} - Domain Provisioned Successfully!"

  echo 'root = administrator' > /etc/samba/smbusers
  # Mark samba as setup
  touch "${INIT_FILE}"
}

# Initialize Samba
initialize_samba() {
  case "$SAMBA_ROLE" in
    primary_domain)
      create_primary_domain_controller
      ;;
    secondary_domain)
      create_secondary_domain_controller
      ;;
    *)
      die "Unknown Expression ..."
      ;;
    esac
}

# Pre-Initialize
pre_initialization() {
  # Fix nameserver
#  echo -e "search ${SAMBA_DC_REALM}\nnameserver 127.0.0.1" > /etc/resolv.conf
#  echo -e "127.0.0.1 ${HOSTNAME} localhost" > /etc/hosts
#  echo -e "${HOSTNAME}" > /etc/hostname

  [[ -d /var/log/samba/cores ]] || mkdir -pv /var/log/samba/cores
  chmod -R 0700 /var/log/samba
}

samba_template() {
  sed -e "s/%NETBIOS_NAME%/$NETBIOS_NAME/g" \
      -e "s/%HOSTNAME%/$(hostname)/g" \
      /templates/smb.conf.tpl > /etc/samba/smb.conf
}

# Start samba
start_samba() {
  log_info "starting samba ..."

  local samba_opts=
  local samba_opts="${samba_opts} --interactive"
  if [[ ${SAMBA_DEBUGLEVEL} -gt 0 ]]
  then
    local samba_opts="${samba_opts} --leak-report"
    local samba_opts="${samba_opts} --leak-report-full"
    local samba_opts="${samba_opts} --debuglevel=${SAMBA_DEBUGLEVEL}"
  fi

  # Configure Samba Conf
  ######################
  samba_template

  # Daemonizing Samba
  ###################
  samba ${samba_opts}
}

start_ntp() {
  log_info "starting ntp ..."
  if [[ -f /etc/ntpd.conf ]]; then
    ntpd -d -f /etc/ntpd.conf &
  fi
  sleep 5
}

main() {

  #  pre_initialization
  if [[ ! -f "$INIT_FILE" ]]; then
    # Check Needed Vars
    if [[ -z $SAMBA_ROLE ]]; then
      die "Failed to provide SAMBA_ROLE Token!"
    fi

    if [[ -z "$SAMBA_DC_REALM" ]]; then
      die "Variable SAMBA_DC_REALM is empty"
    fi

    if [[ -z "$SAMBA_DC_DOMAIN" ]]; then
      die "Variable SAMBA_DC_DOMAIN is empty"
    fi

    if [[ -z $SAMBA_DC_ADMIN_APP_ROLE_ID ]];then
      die "Failed to provide APP ROLE Token!"
    fi

    if [[ -z $SAMBA_DC_ADMIN_APP_ROLE_SECRET ]]; then
      die "Failed to provide APP ROLE SECRET !"
    fi

    initialize_samba
  fi

  start_ntp
  start_samba
}

# Entrypoint
main