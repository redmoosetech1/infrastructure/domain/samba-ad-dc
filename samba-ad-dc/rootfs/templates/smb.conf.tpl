# Global parameters
[global]
        dns forwarder = 172.16.0.19
        netbios name = %NETBIOS_NAME%
        realm = RMT.LOCAL
        server role = active directory domain controller
        workgroup = RMT
        bind interfaces only = yes
        interfaces = lo ens192
        idmap_ldb:use rfc2307 = yes

        tls enabled  = yes
        tls keyfile  = tls/%HOSTNAME%.rmt.local.key
        tls certfile = tls/%HOSTNAME%.rmt.local.crt
        tls cafile   = tls/root_ca.crt

        apply group policies = yes

[sysvol]
        path = /var/lib/samba/sysvol
        read only = No

[netlogon]
        path = /var/lib/samba/sysvol/rmt.local/scripts
        read only = No